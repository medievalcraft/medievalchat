# README #

## General Information ##

**Authors:** Loapu

[![Maven metadata URI](https://img.shields.io/maven-metadata/v/https/repo.medievalsuite.de/repository/medievalsuite/de/medievalcraft/medievalchat/MedievalChat/maven-metadata.xml.svg?style=flat-square)]()
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/medievalcraft/medievalchat.svg?style=flat-square)]()
[![License](https://img.shields.io/badge/license-GPL_v3-green.svg?style=flat-square)]()

This software is a MedievalSuite module.
See (https://www.medievalsuite.de) for further information.

## Versioning ##

This software uses Semantic Versioning.
See (https://semver.org/) for further information.

**development branch** - Contains the latest indev version.

**master branch** - Contains the latest stable version.

**beta-\* branch** - Contains the mentioned beta version.

## Need Help? ##

**Raise an issue:** https://www.medievalsuite.de/issues/charly

**Documentation:** https://www.medievalsuite.de/docs/charly

**JavaDocs:** https://www.medievalsuite.de/javadocs/MedievalChat