/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.listeners;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalUserMeta;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.Team;

import javax.swing.text.html.Option;

public class QuitListener implements Listener
{
	private MedievalChat plugin;
	private MedievalUserMeta meta;
	
	public QuitListener(MedievalChat plugin)
	{
		this.plugin = plugin;
		meta = new MedievalUserMeta();
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e)
	{
		MedievalChat.removeFromPM(e.getPlayer().getName());
		plugin.getServer().getScoreboardManager().getMainScoreboard().getTeam(e.getPlayer().getName().toLowerCase()).unregister();
		plugin.log("Spieler deregistriert!");
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e)
	{
		if (plugin.getServer().getScoreboardManager().getMainScoreboard().getTeam(e.getPlayer().getName().toLowerCase()) == null)
		{
			plugin.getServer().getScoreboardManager().getMainScoreboard().registerNewTeam(e.getPlayer().getName().toLowerCase());
		}
		plugin.getServer().getScoreboardManager().getMainScoreboard().getTeam(e.getPlayer().getName().toLowerCase()).setPrefix(ChatColor.getByChar(meta.getUserMeta(e.getPlayer(),"color")).toString());
		plugin.getServer().getScoreboardManager().getMainScoreboard().getTeam(e.getPlayer().getName().toLowerCase()).addEntry(e.getPlayer().getName());
	}
}
