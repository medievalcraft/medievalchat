/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.listeners;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import de.medievalcraft.medievalchat.MedievalUserMeta;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener
{
	private MedievalChat plugin;
	private MedievalUserMeta meta;
	
	public ChatListener(MedievalChat plugin)
	{
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
		meta = new MedievalUserMeta();
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onChat(AsyncPlayerChatEvent e)
	{
		e.setCancelled(true);
		double r = 30.0;
		String subColor = "";
		String color = "";
		String gender = "";
		//String name = "";
		if (meta.hasMeta(e.getPlayer(), "subcolor")) subColor = "&" + meta.getUserMeta(e.getPlayer(), "subcolor");
		if (meta.hasMeta(e.getPlayer(), "color")) color = "&" + meta.getUserMeta(e.getPlayer(), "color");
		if (e.getPlayer().hasPermission("gender.female"))
		{
			if (meta.hasMeta(e.getPlayer(), "female")) gender = meta.getUserMeta(e.getPlayer(), "female");
		}
		else
		{
			if (meta.hasMeta(e.getPlayer(), "male")) gender = meta.getUserMeta(e.getPlayer(), "male");
		}
		//if (meta.hasMeta(e.getPlayer(), "name")) name = meta.getUserMeta(e.getPlayer(), "name");
		
		if (e.getMessage().toLowerCase().startsWith("*flüstert") || e.getMessage().startsWith("*murmelt")) r = 3;
		if (e.getMessage().toLowerCase().startsWith("*schreit") || e.getMessage().startsWith("*ruft")) r = 60;
		String rawMessage = e.getMessage();
		StringBuilder convertedMessageBuilder = new StringBuilder();
		String convertedMessage;
		if (rawMessage.contains("((")) //Wenn eine Rohnachricht einen OOC-Bereich öffnet, dann...
		{
			boolean isInsideOfOoc = false;
			String[] rawMessageArray = rawMessage.split(" "); //...lege ein Array mit allen Wortschnipseln der Rohnachricht an und...
			for (int i = 0; i < rawMessageArray.length; i++) //...loope durch das Array durch, um dann...
			{
				String s = rawMessageArray[i];
				if (s.contains("((")) //...zu überprüfen, ob in diesem Schnipsel ein OOC-Bereich geöffnet wird, damit...
				{
					String[] tempMessagePiece = s.split("\\(\\("); //...wir ein temporäres Array erstellen können, welches jeglichen Text von den Klammern trennt, sodass...
					switch (tempMessagePiece.length)
					{
						case 0: s = "§7§o((";
							break;
						case 1:
							if (s.endsWith("(("))
							{
								s = tempMessagePiece[0] + " §7§o((";
							}
							else
							{
								s = "§7§o(( " + tempMessagePiece[0];
							}
							break;
						default: s = tempMessagePiece[0] + " §7§o(( §7§o" + tempMessagePiece[1];
					}
					isInsideOfOoc = true;
				}
				if (isInsideOfOoc)
				{
					s = plugin.getNamed(s, "§o", "§7§o");
				}
				if (s.contains("))")) //...zu überprüfen, ob in diesem Schnipsel ein OOC-Bereich geöffnet wird, damit...
				{
					String[] tempMessagePiece = s.split("\\)\\)"); //...wir ein temporäres Array erstellen können, welches jeglichen Text von den Klammern trennt, sodass...
					switch (tempMessagePiece.length)
					{
						case 0: s = "§7§o))§f ";
							break;
						case 1:
							if (s.endsWith("))"))
							{
								s = tempMessagePiece[0] + " §7§o))§f ";
							}
							else
							{
								s = "§7§o)) §f" + tempMessagePiece[0];
							}
							break;
						default: s = tempMessagePiece[0] + " §7§o)) §f" + tempMessagePiece[1];
					}
					isInsideOfOoc = false;
				}
				rawMessageArray[i] = s;
			}
			
			for (String s : rawMessageArray)
			{
				convertedMessageBuilder.append(s).append(" ");
			}
			convertedMessage = convertedMessageBuilder.toString();
			if (!convertedMessage.contains("))"))
			{
				convertedMessage = convertedMessage + "§7§o))§f";
			}
		}
		else
		{
			convertedMessage = rawMessage;
		}
		
		/*if (e.getMessage().contains("((")) //Sobald jemand einen OOC-Bereich öffnet
		{
			Boolean b = false;
			StringBuilder sb = new StringBuilder();
			String[] strArr = e.getMessage().split(" "); //Teile die Nachricht Wort für wort auf
			for (int i = 0; i < strArr.length; i++)
			{
				if (strArr[i].contains("))"))//Prüfe jedes Wort auf den Inhalt von ))
				{
					strArr[i] = strArr[i] + "§f";
					b = false;
				}
				if (b)
				{
					strArr[i] = "§7§o" + plugin.getNamed(strArr[i], "§o", "§7§o");
				}
				if (strArr[i].contains("(("))
				{
					String[] temp = strArr[i].split("\\(\\(");
					
					if (temp.length == 0)
					{
						strArr[i] = "§7§o((";
					}
					if (temp.length == 2)
					{
						strArr[i] = temp[0] + " §7§o(( §7§o" + temp[1];
					}
					if (temp.length == 1)
					{
						if (strArr[i].endsWith("(("))
						{
							strArr[i] = temp[0] + " §7§o((";
						}
						else
						{
							strArr[i] = "§7§o(( " + temp[0];
						}
					}
					//strArr[i] = strArr[i].substring(0, strArr[i].length() - 2) + " §7§o((";
					b = true;
				}
				sb.append(strArr[i]).append(" ");
			}
			e.setMessage(sb.toString());
			if (e.getMessage().contains("))"))
			{
				e.setMessage(e.getMessage().replaceAll("\\)\\)", "))§f"));
			}
			else
			{
				e.setMessage(e.getMessage() + "))");
			}
			
			if (e.getMessage().contains("(("))
			{
				e.setMessage(e.getMessage().replaceAll("\\(\\(", "§7(("));
			}
			else
			{
				e.setMessage(e.getMessage() + "))");
			}
		}*/
		if (convertedMessage.startsWith(" "))
		{
			convertedMessage = convertedMessage.substring(1);
		}
		String text = "&f" + subColor + "[" + gender + "]&7" + color + e.getPlayer().getName() + "&f: ";
		text = ChatColor.translateAlternateColorCodes('&', text);
		e.setMessage(convertedMessage);
		if (e.getPlayer().hasPermission(MedievalPerms.NAMED.getPerm()))
		{
			e.setMessage(plugin.getNamed(e.getMessage()));
		}
		if (e.getPlayer().hasPermission(MedievalPerms.COLOR.getPerm()))
		{
			e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
		}
		for (Entity entity : e.getPlayer().getNearbyEntities(r, r, r))
		{
			if (entity instanceof Player)
			{
				Player target = (Player) entity;
				target.sendMessage(text + e.getMessage());
			}
		}
		e.getPlayer().sendMessage(text + e.getMessage());
		Bukkit.getConsoleSender().sendMessage(text + e.getMessage());
	}
}
