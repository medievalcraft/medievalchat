/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat;

import de.medievalcraft.medievalchat.commands.AntwortenCommand;
import de.medievalcraft.medievalchat.commands.ChannelCommand;
import de.medievalcraft.medievalchat.commands.FertigCommand;
import de.medievalcraft.medievalchat.commands.MedievalchatCommand;
import de.medievalcraft.medievalchat.commands.MsgtoggleCommand;
import de.medievalcraft.medievalchat.commands.SchreibenCommand;
import de.medievalcraft.medievalchat.listeners.ChatListener;
import de.medievalcraft.medievalchat.listeners.QuitListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MedievalChat extends MedievalPlugin
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private YamlConfiguration config;
	private File f;
	public static Map<String, String> inPrivateChat = new HashMap<>();
	public HashMap<String, List<String>> hasBlocked = new HashMap<>();
	public String prefix = "&8[&6" + this.getDescription().getPrefix() + "&8] &7";
	private MedievalUserMeta meta = null;
	private MedievalUpdater updater = null;
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static MedievalChat i;
	public static MedievalChat get() { return i; }
	public MedievalChat() { i = this; }
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnableInner()
	{
		config();
		loadListeners();
		registerCommands();
		meta = new MedievalUserMeta();
		
		for (Player p : Bukkit.getOnlinePlayers())
		{
			Team team = getServer().getScoreboardManager().getMainScoreboard().getTeam(p.getName().toLowerCase());
			
			if (team != null)
			{
				team.unregister();
			}
			getServer().getScoreboardManager().getMainScoreboard().registerNewTeam(p.getName().toLowerCase());
			Team team2 = getServer().getScoreboardManager().getMainScoreboard().getTeam(p.getName().toLowerCase());
			team2.setPrefix(ChatColor.getByChar(meta.getUserMeta(p,"color")).toString());
			team2.addEntry(p.getName());
			
		}
		
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				for (Player p : Bukkit.getOnlinePlayers())
				{
					if (getServer().getScoreboardManager().getMainScoreboard().getTeam(p.getName().toLowerCase()) == null)
					{
						getServer().getScoreboardManager().getMainScoreboard().registerNewTeam(p.getName().toLowerCase());
					}
					getServer().getScoreboardManager().getMainScoreboard().getTeam(p.getName().toLowerCase()).setPrefix(ChatColor.getByChar(meta.getUserMeta(p,"color")).toString());
					getServer().getScoreboardManager().getMainScoreboard().getTeam(p.getName().toLowerCase()).addEntry(p.getName());
				}
			}
		}.runTaskTimer(this, 0L, 6000L);
	}
	
	private void loadListeners()
	{
		new ChatListener(get());
		new QuitListener(get());
		updater = new MedievalUpdater();
		Bukkit.getPluginManager().registerEvents(updater, this);
	}
	
	private void registerCommands()
	{
		SchreibenCommand msg = new SchreibenCommand(get());
		getCommand("schreiben").setExecutor(msg);
		
		AntwortenCommand r = new AntwortenCommand(get());
		getCommand("antworten").setExecutor(r);
		
		MsgtoggleCommand mtog = new MsgtoggleCommand(get());
		getCommand("msgtoggle").setExecutor(mtog);
		
		FertigCommand fertig = new FertigCommand(get());
		getCommand("fertig").setExecutor(fertig);
		
		MedievalchatCommand mc = new MedievalchatCommand(get());
		getCommand("medievalchat").setExecutor(mc);
		
		for (String s: this.config.getConfigurationSection("Channels").getKeys(false))
		{
			registerNewChannel(s.toLowerCase(), "Chat inside the " + s + "-Channel", "/" + s, this.config.getStringList("Channels." + s + ".aliases"));
		}
	}
	
	private void registerNewChannel(String cmdLabel, String description, String usage, List<String> aliases)
	{
		try
		{
			Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			
			bukkitCommandMap.setAccessible(true);
			CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
			
			commandMap.register("medievalchat", new ChannelCommand(cmdLabel, description, usage, aliases));
			
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
		{
			//TODO Handle exceptions
			e.printStackTrace();
		}
	}
	
	public void reloadConfig()
	{
		config();
	}
	
	@Override
	public YamlConfiguration getConfig()
	{
		return config;
	}
	
	private void config()
	{
		if (getDataFolder().mkdirs()) log("Pluginverzeichnis wurde erstellt.");
		this.f = new File(getDataFolder(), "config.yml");
		if (!this.f.exists())
		{
			try
			{
				Files.copy(getResource("config.yml"), this.f.toPath());
			}
			catch (Exception localException)
			{
				localException.printStackTrace();
			}
		}
		this.config = YamlConfiguration.loadConfiguration(this.f);
		if (this.config.getString("MSG-System.Zu Spieler") == null)
		{
			this.config.set("MSG-System.Zu Spieler", String.valueOf("&a(Zu %Empfänger): &7"));
		}
		if (this.config.getString("MSG-System.Von Spieler") == null)
		{
			this.config.set("MSG-System.Von Spieler", String.valueOf("&a(Von %Sender): &7"));
		}
		if (this.config.get("Updater.Aktiviert") == null)
		{
			this.config.set("Updater.Aktiviert", Boolean.valueOf("true"));
		}
		if (this.config.getString("Updater.Kanal") == null)
		{
			this.config.set("Updater.Kanal", String.valueOf("stable"));
		}
		if (this.config.getConfigurationSection("Channels") == null)
		{
			this.config.set("Channels.custom.name", String.valueOf("Custom Channel"));
			this.config.set("Channels.custom.format", String.valueOf("&3&l"));
			List<String> list = new ArrayList<>();
			list.add("customcommand1");
			list.add("customcommand2");
			this.config.set("Channels.custom.aliases", list);
		}
		sc();
		this.config = YamlConfiguration.loadConfiguration(this.f);
	}
	
	private void sc()
	{
		try
		{
			this.config.save(this.f);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Stellt eine private Nachricht zwischen zwei Spielern zu.
	 *
	 *
	 * @param p1
	 *            Name von Spieler 1 (Sender)
	 * @param p2
	 *            Name von Spieler 2 (Empf�nger)
	 * @param text
	 *            Zuzustellender Text
	 */
	public void privateMessage(CommandSender p1, CommandSender p2, String text)
	{
		String to = config.getString("MSG-System.Zu Spieler");
		String from = config.getString("MSG-System.Von Spieler");
		
		to = to.replace("%Empfänger", p2.getName());
		to = to.replace("%Sender", p1.getName());
		
		from = from.replace("%Empfänger", p2.getName());
		from = from.replace("%Sender", p1.getName());
		
		sendMessage(p1, to + text);
		sendMessage(p2, from + text);
	}
	
	/**
	 * Setzt den PN-Status von Spielern.
	 *
	 * @param p1
	 *            Spieler 1
	 * @param p2
	 *            Spieler 2
	 */
	public static void setInPm(String p1, String p2)
	{
		if (inPrivateChat == null)
		{
			inPrivateChat = new HashMap<>();
		}
		inPrivateChat.put(p1, p2);
	}
	
	/**
	 * Nimm den PN-Status von Spielern.
	 *
	 * @param p1
	 *            Spieler 1
	 */
	public static void removeFromPM(String p1)
	{
		if (inPrivateChat == null)
		{
			return;
		}
		inPrivateChat.remove(p1);
	}
	
	/**
	 * Gibt den Chatpartner von Spieler 1 an.
	 *
	 * @param p1
	 *            Spieler 1
	 * @return Chatpartner von Spieler 1
	 */
	public static String getPmRecipient(String p1)
	{
		if (inPrivateChat == null)
		{
			return null;
		}
		if (inPrivateChat.containsKey(p1))
		{
			return inPrivateChat.get(p1);
		}
		return null;
	}
	
	public void sendMessage(CommandSender sender, String msg)
	{
		sender.sendMessage(msg.replaceAll("&", "\u00A7"));
	}
	
	public void sendMessage(String msg, String permission)
	{
		for (Player p : Bukkit.getOnlinePlayers())
		{
			if (p.hasPermission(permission))
			{
				sendMessage(p, msg);
			}
		}
	}
	public String getNamed(String msg, String pre, String post)
	{
		String[] s = msg.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length; i++)
		{
			Player p = Bukkit.getPlayer(s[i]);
			if (p != null)
			{
				if (p.getName().equalsIgnoreCase(s[i]))
				{
					String color = meta.getUserMeta(p, "color");
					s[i] = "§" + color + pre + p.getName() + post;
				}
			}
			if (i == s.length - 1)
			{
				sb.append(s[i]);
			}
			else
			{
				sb.append(s[i]).append(" ");
			}
		}
		return sb.toString();
	}
	public String getNamed(String msg)
	{
		return getNamed(msg, "", "§f");
	}
	
	public String getNamed(String msg, String post)
	{
		return getNamed(msg, "", post);
	}
}
