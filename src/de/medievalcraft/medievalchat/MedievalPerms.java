/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat;

public enum MedievalPerms
{
	BASECOMMAND("basecommand"),
	HOCHADEL("hochadel"),
	MINISTERIUM("ministerium"),
	MSGTOGGLE("msgtoggle"),
	FERTIG("fertig"),
	ANTWORTEN("antworten"),
	SCHREIBEN("schreiben"),
	RELOAD("reload"),
	NAMED("named"),
	COLOR("color");
	
	private String node;
	MedievalPerms(String node) { this.node = node; }
	
	public String getPerm()
	{
		return "medievalchat." + node;
	}
}
