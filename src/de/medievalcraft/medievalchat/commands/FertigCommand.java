/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FertigCommand implements CommandExecutor
{
	private MedievalChat plugin;
	
	public FertigCommand(MedievalChat plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		Player p = null;
		if (sender instanceof Player)
		{
			p = (Player) sender;
		}
		
		if (p == null)
		{
			System.out.println("Dieser Befehl kann nur von Spielern ausgeführt werden!");
			return true;
		}
		
		if (!p.hasPermission(MedievalPerms.FERTIG.getPerm()))
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDafür fehlen dir die Berechtigungen");
			return true;
		}
		
		if (!MedievalChat.inPrivateChat.containsKey(p.getName()))
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDu hast keine offenen Konversation, die es zu schließen gilt!");
			return true;
		}
		
		if (!MedievalChat.inPrivateChat.containsValue(p.getName()))
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDer Spieler " + MedievalChat.inPrivateChat.get(p.getName()) + " ist bereits in einer neuen Konversation.");
			MedievalChat.removeFromPM(p.getName());
			return true;
		}
		
		MedievalChat.removeFromPM(MedievalChat.inPrivateChat.get(p.getName()));
		plugin.sendMessage(sender, plugin.prefix + "&aDeine Konversation mit " + MedievalChat.inPrivateChat.get(p.getName()) + " wurde geschlossen.");
		plugin.sendMessage(Bukkit.getPlayer(MedievalChat.inPrivateChat.get(p.getName())), plugin.prefix + "&aDer Spieler " + p.getName() + " hat die Konversation mit dir geschlossen.");
		
		return true;
	}
}
