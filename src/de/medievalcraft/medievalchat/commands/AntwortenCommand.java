/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AntwortenCommand implements CommandExecutor
{
	private MedievalChat plugin;
	
	public AntwortenCommand(MedievalChat plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		if (!sender.hasPermission(MedievalPerms.ANTWORTEN.getPerm()))
		{
			Player p = null;
			if (sender instanceof Player)
			{
				p = (Player) sender;
			}
			TextComponent brackets = new TextComponent("[");
			brackets.setColor(ChatColor.DARK_GRAY);
			
			TextComponent prefix = new TextComponent("Charly");
			prefix.setColor(ChatColor.GOLD);
			
			brackets.addExtra(prefix);
			brackets.addExtra("] ");
			
			TextComponent message = new TextComponent("Wenn du hemanden erreichen möchtest, so benutze doch einen Brief. ");
			message.setColor(ChatColor.RED);
			
			TextComponent callToAction = new TextComponent("Wie geht das? (Klicken)");
			callToAction.setColor(ChatColor.GREEN);
			callToAction.setBold(true);
			callToAction.setClickEvent(new ClickEvent(Action.OPEN_URL, "http://www.medievalcraft.de/wiki/index.php?title=Briefe"));
			callToAction.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Öffne die Wikiseite für Briefe!").create()));
			
			brackets.addExtra(message);
			brackets.addExtra(callToAction);
			
			if (p != null)
			{
				p.spigot().sendMessage(brackets);
			}
			return true;
		}
		if (MedievalChat.getPmRecipient(sender.getName()) == null)
		{
			plugin.sendMessage(sender, plugin.prefix + "&cEs hat dich niemand angeschrieben!");
			return true;
		}
		
		if (args.length < 1)
		{
			plugin.sendMessage(sender, plugin.prefix + "&cFalsche Benutzung: &a/r [Nachricht]");
			return true;
		}
		
		String user = MedievalChat.getPmRecipient(sender.getName());
		
		StringBuilder sb = new StringBuilder();
		
		for (String s : args)
		{
			sb.append(s).append(" ");
		}
		
		String msg = sb.toString();
		
		if (!sender.hasPermission(MedievalPerms.COLOR.getPerm()))
		{
			msg = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', msg));
		}
		
		CommandSender u = Bukkit.getPlayer(user);
		
		if (user != null && user.equalsIgnoreCase("console")) u = Bukkit.getConsoleSender();
		
		if (u == null)
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDieser Spieler ist nicht online!");
			MedievalChat.removeFromPM(sender.getName());
			return true;
		}
		
		if (u.getName().equals(sender.getName()))
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDu kannst dir nicht selber schreiben!");
			return true;
		}
		
		MedievalChat.setInPm(sender.getName(), u.getName());
		MedievalChat.setInPm(u.getName(), sender.getName());
		
		plugin.privateMessage(sender, u, plugin.getNamed(msg, "§7"));
		
		return true;
	}
}
