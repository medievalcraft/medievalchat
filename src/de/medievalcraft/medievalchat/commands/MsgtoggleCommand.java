/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class MsgtoggleCommand implements CommandExecutor
{
	private MedievalChat plugin;
	
	public MsgtoggleCommand(MedievalChat plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		Player p = null;
		String playerName = null;
		
		if (sender instanceof Player)
		{
			
			p = (Player) sender;
			playerName = p.getName();
			
		}
		
		if (p == null)
		{
			
			System.out.println("Dieser Befehl kann nur von Spielern ausgeführt werden!");
			return true;
			
		}
		
		if (!p.hasPermission(MedievalPerms.MSGTOGGLE.getPerm()))
		{
			
			plugin.sendMessage(sender, plugin.prefix + "&cDu hast nicht die dafür benötigte Berechtigung!");
			return true;
			
		}
		
		if (args.length < 1)
		{
			
			plugin.sendMessage(sender, plugin.prefix + "&cBitte gib den Spieler an, den du ent-/blocken möchtest.");
			return true;
			
		}
		
		Player user = Bukkit.getPlayer(args[0]);
		
		if (user == null)
		{
			
			plugin.sendMessage(sender, plugin.prefix + "&cDer Spieler " + args[0] + " ist derzeit nicht online!");
			return true;
			
		}
		
		if (user.getName().equals(playerName))
		{
			
			plugin.sendMessage(sender, plugin.prefix + "&cDu kannst dich nicht selber blocken!");
			return true;
			
		}
		
		List<String> list = new ArrayList<>();
		
		list.add(user.getName());
		
		String state = " geblockt!";
		
		if (!plugin.hasBlocked.containsKey(playerName))
		{
			
			plugin.hasBlocked.put(playerName, list);
			plugin.sendMessage(sender, plugin.prefix + "&aDu hast " + user.getName() + state);
			
		}
		else
		{
			
			list.clear();
			list = plugin.hasBlocked.get(playerName);
			
			if (list.contains(user.getName()))
			{
				
				list.remove(user.getName());
				state = " entblockt!";
				
			}
			else
			{
				
				list.add(user.getName());
				
			}
			
			plugin.sendMessage(sender, plugin.prefix + "&aDu hast " + user.getName() + state);
			plugin.hasBlocked.put(playerName, list);
			
		}
		return true;
	}
}
