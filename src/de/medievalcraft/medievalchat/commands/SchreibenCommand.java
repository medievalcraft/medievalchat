/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class SchreibenCommand implements CommandExecutor
{
	private MedievalChat plugin;
	
	public SchreibenCommand(MedievalChat plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		
		if (!sender.hasPermission(MedievalPerms.SCHREIBEN.getPerm()))
		{
			Player p = null;
			if (sender instanceof Player)
			{
				p = (Player) sender;
			}
			TextComponent brackets = new TextComponent("[");
			brackets.setColor(net.md_5.bungee.api.ChatColor.DARK_GRAY);
			
			TextComponent prefix = new TextComponent("Charly");
			prefix.setColor(net.md_5.bungee.api.ChatColor.GOLD);
			
			brackets.addExtra(prefix);
			brackets.addExtra("] ");
			
			TextComponent message = new TextComponent("Wenn du jemanden erreichen möchtest, so benutze doch einen Brief. ");
			message.setColor(net.md_5.bungee.api.ChatColor.RED);
			
			TextComponent callToAction = new TextComponent("Wie geht das? (Klicken)");
			callToAction.setColor(net.md_5.bungee.api.ChatColor.GREEN);
			callToAction.setBold(true);
			callToAction.setClickEvent(new ClickEvent(Action.OPEN_URL, "http://www.medievalcraft.de/wiki/index.php?title=Briefe"));
			callToAction.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Öffne die Wikiseite für Briefe!").create()));
			
			brackets.addExtra(message);
			brackets.addExtra(callToAction);
			
			if (p != null)
			{
				p.spigot().sendMessage(brackets);
			}
			return true;
		}
		
		if (args.length < 2)
		{
			plugin.sendMessage(sender, plugin.prefix + "&cFalsche Benutzung: &a/msg <Spieler> <Nachricht>");
			return true;
		}
		
		CommandSender user = Bukkit.getPlayer(args[0]);
		
		if (args[0].equalsIgnoreCase("console")) user = Bukkit.getConsoleSender();
		
		if (user == null)
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDer Spieler " + args[0] + " ist derzeit nicht online!");
			return true;
		}
		
		if (user.getName().equals(sender.getName()))
		{
			plugin.sendMessage(sender, plugin.prefix +  "&cDu kannst dir nicht selber schreiben!");
			return true;
		}
		
		if (plugin.hasBlocked.containsKey(user.getName()))
		{
			
			List<String> list = plugin.hasBlocked.get(user.getName());
			
			if (list.contains(sender.getName()))
			{
				
				plugin.sendMessage(sender, plugin.prefix + "&cDieser Spieler hat dich blockiert!");
				return true;
				
			}
			
		}
		
		MedievalChat.setInPm(sender.getName(), user.getName());
		MedievalChat.setInPm(user.getName(), sender.getName());
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 1; i < args.length; i++)
		{
			sb.append(args[i]).append(" ");
		}
		
		String msg = sb.toString();
		
		if (!sender.hasPermission(MedievalPerms.COLOR.getPerm()))
		{
			msg = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', msg));
		}
		
		plugin.privateMessage(sender, user, plugin.getNamed(msg, "§7"));
		
		if (!(user instanceof Player)) return true;
		final Player user2 = (Player) user;
		
		Bukkit.getScheduler().runTaskAsynchronously(plugin,() ->
		{
				user2.playSound(user2.getLocation(), Sound.ENTITY_EVOKER_PREPARE_WOLOLO, 1, 1);
				try
				{
					Thread.sleep(800);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				user2.playSound(user2.getLocation(), Sound.ENTITY_EVOKER_PREPARE_WOLOLO, 1, 0.2F);
		});
		
		return true;
	}
}
