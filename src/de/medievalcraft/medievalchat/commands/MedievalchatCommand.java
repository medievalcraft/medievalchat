/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MedievalchatCommand implements CommandExecutor
{
	private MedievalChat plugin;
	
	public MedievalchatCommand(MedievalChat plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args)
	{
		
		if (!sender.hasPermission(MedievalPerms.BASECOMMAND.getPerm()))
		{
			plugin.sendMessage(sender, plugin.prefix + "&cDafür fehlen dir die Berechtigungen");
			return true;
		}
		
		if (args.length > 0)
		{
			if (args[0].equalsIgnoreCase("reload") && sender.hasPermission(MedievalPerms.RELOAD.getPerm()))
			{
				plugin.reloadConfig();
				plugin.sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
				plugin.sendMessage(sender, "&7» Die Konfigurationsdatei von &6" + plugin.getDescription().getName() + " &7wurde neu geladen.");
				plugin.sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
				return true;
			}
			info(sender, cmdLabel);
			return true;
		}
		info(sender, cmdLabel);
		return true;
	}
	
	private void info(CommandSender sender, String cmdLabel)
	{
		plugin.sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
		plugin.sendMessage(sender, "&7» &6" + plugin.getDescription().getName() + " &8(&6" + plugin.getDescription().getPrefix() + "&8) &7v" + plugin.getDescription().getVersion());
		plugin.sendMessage(sender, "&7» Geschrieben von: &6" + plugin.getDescription().getAuthors());
		plugin.sendMessage(sender, "&7» Dokumentation: &6" + plugin.getDescription().getWebsite());
		if (sender.hasPermission(MedievalPerms.RELOAD.getPerm()))
		{
			plugin.sendMessage(sender, "&7» Konfig neu laden: &6/" + cmdLabel + " reload");
		}
		plugin.sendMessage(sender, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
		
	}
}
