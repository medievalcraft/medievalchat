/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat.commands;

import de.medievalcraft.medievalchat.MedievalChat;
import de.medievalcraft.medievalchat.MedievalPerms;
import de.medievalcraft.medievalchat.MedievalUserMeta;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.List;

public class ChannelCommand extends BukkitCommand
{
	private MedievalUserMeta meta;
	private String commandName;
	
	public ChannelCommand(String name, String description, String usageMessage, List<String> aliases)
	{
		super(name.replaceAll("/", ""), description, usageMessage, aliases);
		meta = new MedievalUserMeta();
		commandName = name;
	}
	
	private MedievalChat charly = MedievalChat.get();
	
	@Override
	public boolean execute(CommandSender sender, String cmdLabel, String[] args)
	{
		Player p = null;
		String playerName = null;
		
		if (sender instanceof Player)
		{
			
			p = (Player) sender;
			playerName = p.getName();
			
		}
		
		if (p == null)
		{
			
			System.out.println("Dieser Befehl kann nur von Spielern ausgeführt werden!");
			return true;
			
		}
		
		if (!p.hasPermission("medievalchat.channel." + commandName.toLowerCase()))
		{
			
			charly.sendMessage(sender, charly.prefix + "&cDu hast nicht die dafür benötigte Berechtigung!");
			return true;
			
		}
		
		if (args.length == 0)
		{
			
			charly.sendMessage(sender, charly.prefix + "&cBitte gib die Nachricht ein, die du schreiben möchtest!");
			return true;
			
		}
		
		StringBuilder msg = new StringBuilder();
		
		for (String s : args)
		{
			
			msg.append(s).append(" ");
			
		}
		
		String msg1 = msg.toString();
		
		if (!sender.hasPermission(MedievalPerms.COLOR.getPerm()))
		{
			msg1 = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', msg1));
		}
		
		String color = meta.hasMeta(p, "color") ? "&" + meta.getUserMeta(p, "color") : "";
		
		String prefix = "";
		
		for (String s : charly.getConfig().getConfigurationSection("Channels").getKeys(false))
		{
			if (s.equalsIgnoreCase(commandName))
			{
				prefix = charly.getConfig().getString("Channels." + s + ".format") + charly.getConfig().getString("Channels." + s + ".name");
			}
		}
		
		charly.sendMessage("&8&l[" + prefix + "&8&l]&r " + color + playerName + "&f: " + charly.getNamed(msg1), "medievalchat.channel." + commandName.toLowerCase());
		
		return true;
	}

}
