/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat;

public class MedievalVersion
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private int stableMajor;
	private int stableMinor;
	private int stablePatch;
	
	private int betaMajor;
	private int betaMinor;
	private int betaPatch;
	private String betaPre;
	
	private String inDevPre;
	private int inDevBuild;
	
	private String dot = ".";
	private String minus = "-";
	private String plus = "+";
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static MedievalVersion i;
	public static MedievalVersion get() { return i; }
	public MedievalVersion() { i = this; }
	public MedievalVersion(String version)
	{
		this.readFromString(version);
	}
	
	// -------------------------------------------- //
	// GETTER & SETTER
	// -------------------------------------------- //
	
	public int getStableMajor() { return stableMajor; }
	public int getStableMinor() { return stableMinor; }
	public int getStablePatch() { return stablePatch; }
	
	public int getBetaMajor() { return betaMajor; }
	public int getBetaMinor() { return betaMinor; }
	public int getBetaPatch() { return betaPatch; }
	public String getBetaPre() { return betaPre; }
	
	public String getInDevPre() { return inDevPre; }
	public int getInDevBuild() { return inDevBuild; }
	
	public void setStableMajor(int stableMajor)
	{
		this.stableMajor = stableMajor;
	}
	
	public void setStableMinor(int stableMinor)
	{
		this.stableMinor = stableMinor;
	}
	
	public void setStablePatch(int stablePatch)
	{
		this.stablePatch = stablePatch;
	}
	
	public void setBetaMajor(int betaMajor)
	{
		this.betaMajor = betaMajor;
	}
	
	public void setBetaMinor(int betaMinor)
	{
		this.betaMinor = betaMinor;
	}
	
	public void setBetaPatch(int betaPatch)
	{
		this.betaPatch = betaPatch;
	}
	
	public void setBetaPre(String betaPre)
	{
		this.betaPre = betaPre;
	}
	
	public void setInDevPre(String inDevPre)
	{
		this.inDevPre = inDevPre;
	}
	
	public void setInDevBuild(int inDevBuild)
	{
		this.inDevBuild = inDevBuild;
	}
	
	public String getStable()
	{
		return "" + getStableMajor() + dot + getStableMinor() + dot + getStablePatch();
	}
	
	public String getBeta()
	{
		return "" + getBetaMajor() + dot + getBetaMinor() + dot + getBetaPatch() + minus + getBetaPre();
	}
	
	public String getInDev()
	{
		return "" + getBetaMajor() + dot + getBetaMinor() + dot + getBetaPatch() + minus + getInDevPre() + plus + getInDevBuild();
	}
	
	public void setStable(String stable)
	{
		String[] stableArr = stable.split("\\" + dot);
		if (stableArr.length < 3) { return; }
		this.setStableMajor(Integer.parseInt(stableArr[0]));
		this.setStableMinor(Integer.parseInt(stableArr[1]));
		this.setStablePatch(Integer.parseInt(stableArr[2]));
	}
	
	public void setBeta(String beta)
	{
		String[] betaArr = beta.split(minus);
		
		if (betaArr.length != 2) return;
		
		String pre = betaArr[1];
		betaArr = betaArr[0].split("\\" + dot);
		
		
		if (betaArr.length < 3) return;
		
		this.setBetaMajor(Integer.parseInt(betaArr[0]));
		this.setBetaMinor(Integer.parseInt(betaArr[1]));
		this.setBetaPatch(Integer.parseInt(betaArr[2]));
		this.setBetaPre(pre);
	}
	
	public void setInDev(String indev)
	{
		String[][] indevArr = {indev.split(minus), null};
		indevArr[1] = indevArr[0][1].split("\\" + plus);
		
		if (indevArr[0].length != 2) return;
		
		String pre = indevArr[1][0];
		String build = indevArr[1][1];
		indevArr[0] = indevArr[0][0].split("\\" + dot);
		
		if (indevArr[0].length < 3) return;
		
		this.setBetaMajor(Integer.parseInt(indevArr[0][0]));
		this.setBetaMinor(Integer.parseInt(indevArr[0][1]));
		this.setBetaPatch(Integer.parseInt(indevArr[0][2]));
		this.setInDevPre(pre);
		this.setInDevBuild(Integer.parseInt(build));
	}
	
	public String getChannel(String channel)
	{
		if (channel.equalsIgnoreCase("stable")) { return getStable(); }
		if (channel.equalsIgnoreCase("beta")) { return getBeta(); }
		if (channel.equalsIgnoreCase("indev")) { return getInDev(); }
		return null;
	}
	
	// -------------------------------------------- //
	// METHODS
	// -------------------------------------------- //
	
	public void readFromString(String version)
	{
		String[] versionArr = version.split(":");
		String stable = versionArr[0];
		String beta = null;
		String indev = null;
		if (versionArr.length > 1)
		{
			beta = versionArr[1];
		}
		if (versionArr.length > 2)
		{
			indev = versionArr[2];
		}
		this.setStable(stable);
		this.setBeta(beta);
		this.setInDev(indev);
	}
}
