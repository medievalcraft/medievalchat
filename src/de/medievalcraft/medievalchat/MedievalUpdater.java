/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MedievalUpdater implements Listener
{
	private String getLastSpigotVersion()
	{
		String version = "";
		try
		{
			URL url = new URL("https://www.medievalsuite.de/suite-versions/charly.txt");
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
			InputStream inputStream = connection.getInputStream();
			InputStreamReader reader = new InputStreamReader(inputStream);
			BufferedReader in = new BufferedReader(reader);
			version = in.readLine();
			in.close();
			reader.close();
			inputStream.close();
		}
		catch (Exception ex)
		{
			System.out.println("[Charly] Fehler bei der Suche nach neuem Update.");
			ex.printStackTrace();
		}
		return version;
	}
	
	@EventHandler
	public void join(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		if (!p.isOp()) { return; }
		
		Bukkit.getScheduler().runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("MedievalChat"), () -> {
			if (!MedievalChat.get().getConfig().getBoolean("Updater.Aktiviert"))
				return;
			String version = getLastSpigotVersion();
			if (version == null)
				return;
			MedievalVersion v = new MedievalVersion(version);
			String channel = MedievalChat.get().getConfig().getString("Updater.Kanal");
			version = v.getChannel(channel);
			if (!version.equalsIgnoreCase(MedievalChat.get().getDescription().getVersion()))
			{
				MedievalChat.get().sendMessage(p, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
				MedievalChat.get().sendMessage(p, "&7» Ein Update für &6" + MedievalChat.get().getDescription().getName() + " &7im &6" + channel + "-Kanal &7wurde gefunden.");
				MedievalChat.get().sendMessage(p, "&7» Derzeitige Version: &6" + MedievalChat.get().getDescription().getVersion() + " &7Neue Version: &6" + version);
				TextComponent message = new TextComponent("» ");
				message.setColor(ChatColor.GRAY);
				
				TextComponent link = new TextComponent("[Klicke hier, um die neuste Version herunterzuladen]");
				link.setColor(ChatColor.GOLD);
				link.setClickEvent(new ClickEvent(Action.OPEN_URL, "https://bitbucket.org/medievalcraft/medievalchat/downloads/MedievalChat-" + version + ".jar"));
				link.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Starte den Download von MedievalChat.").create()));
				
				message.addExtra(link);
				
				p.spigot().sendMessage(message);
				
				if (MedievalChat.get().getServer().getPluginManager().isPluginEnabled("PlugMan"))
				{
					TextComponent message2 = new TextComponent("» ");
					message2.setColor(ChatColor.GRAY);
					
					TextComponent command = new TextComponent("[Fertig? Klicke hier, um die neue Version zu laden]");
					command.setColor(ChatColor.GOLD);
					command.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/plugman reload medievalchat"));
					command.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Da du PlugMan installiert hast, kannst du MedievalChat gleich neu laden!").create()));
					
					message2.addExtra(command);
					p.spigot().sendMessage(message2);
				}
				MedievalChat.get().sendMessage(p, "&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---&6&m---&8&m---");
			}
		});
	}
}
