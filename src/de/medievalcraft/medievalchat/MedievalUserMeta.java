/*
 *     Copyright © 2018 MedievalCraft.de
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.medievalcraft.medievalchat;

import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicesManager;

import java.util.Objects;

public class MedievalUserMeta
{
	
	private static MedievalUserMeta i;
	//public static MedievalUserMeta get() { return i; }
	public MedievalUserMeta() { i = this; }
	
	private LuckPermsApi getAPI()
	{
		ServicesManager manager = Bukkit.getServicesManager();
		if (manager.isProvidedFor(LuckPermsApi.class))
		{
			return manager.getRegistration(LuckPermsApi.class).getProvider();
		}
		return null;
	}
	
	private User getUser(Player p)
	{
		return Objects.requireNonNull(this.getAPI()).getUserSafe(p.getUniqueId()).orElse(null);
	}
	
	private Contexts getContexts(Player p)
	{
		return Objects.requireNonNull(this.getAPI()).getContextForUser(this.getUser(p)).orElse(null);
	}
	
	private MetaData getMetaData(Player p)
	{
		return this.getUser(p).getCachedData().getMetaData(this.getContexts(p));
	}
	
	public boolean hasMeta(Player p, String meta)
	{
		return this.getMetaData(p).getMeta().containsKey(meta);
	}
	
	public String getUserMeta(Player p, String meta)
	{
		if (this.hasMeta(p, meta))
		{
			return this.getMetaData(p).getMeta().get(meta);
		}
		return null;
	}
}
